<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __contruct(){
        $this->middleware('IsAdmin');
    }

    public function index(){
        return "you are an administrator bcoz you are seeing this page";
    }

}
